# Statics Template

A very quick way to start static-ing a website.

## A case for statics first

When I skip statics, I end up multi-tasking between styling issues and functional issues, prolonging both aspects of development. The typical turn-around for a basic brochure website is about 2-4 weeks. On the last website I skipped statics, the turn-around was about six weeks. The last website I static-ed and then integrated into its platform, took me three weeks. The biggest hurdle with writing static first is developing it so that it transitions easily into a platform (e.g. PHP, React, etc.). Otherwise, time is being wasted writing things twice. This static template allows elements and files to be separated into reusable components, making the transition to a framework more seamless and less about refactoring.

## Setup

* `cd gulp; npm install` (might need `sudo` and/or `-unsafe-perm`)
* Configure Gulp and SCSS
* Run `gulp`
* Use `jQuery(selector).load(path)` to create a global layout(s)

## Helps

### Javascript

A Javascript boilerplate is located at `gulp/js/base.js`, which includes a set of helpful functions:

* `capitalize` - Capitalizes multi-word strings. Useful for titles.
* `cookieGet`, `cookieRemove`, `cookieSet`
* `debounce` (e.g. `debounce(function(){}, 100)`) - Prevents the function from triggering until the specified time has passed. Useful in cases where you want to wait for the user to stop an action before calling a function.
* `didNotClick` - Both jQuery and regular options available.
* `getParams` - Returns an object of the URL's query properties and values.
* `throttle` (e.g. `throttle(function(){}, 100)`) - Limits the frequency of function triggering to the specified time. Useful in cases where overuse of a function may impact performance (e.g. scroll listeners).

### SCSS

An SCSS boilerplate is located at `gulp/scss/setup.scss`.

* The `mob`, `tab`, and `des` functions convert pixel values to `vw` values that correspond to the media queries below. To use these functions, set your browser window to the specified width, and wrap your pixel values (e.g. `10px` => `mob(10px)`).
* The media query values are related to screen widths of popular devices, namely Apple devices.

## Tips

* Responsive scaling with the `mob`, `tab`, and `des` functions occurs automatically, making it preferable. Use them in every instance where a pixel value would be.
* I use a Google Chrome extension called PerfectPixel by WellDoneCode. With the extension, I add design images (exported to the media query width), and it overlays the image with half opacity, so you can write styles to match designs almost perfectly.
* Another useful Google Chrome extension is called Ruler by Vishal Singh. Ruler displays widths, heights, margins, etc.
