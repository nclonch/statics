// const _copy = value => {
//   if (typeof value !== 'object' || value === null) {
//     return value;
//   }

//   if (Array.isArray(value)) {
//     return _copyArray(value);
//   }

//   return _copyObject(value);
// }

// const _copyArray = array => {
//   return array.map(value => {
//     if (value.node) {
//       return _copy(value.node);
//     } else {
//       return _copy(value);
//     }
//   })
// }

// const _copyObject = object => {
//   const result = {};

//   Object.keys(object).forEach((key) => {
//     const value = object[key];

//     if (value && value.edges) {
//       result[key] = _copy(value.edges);
//     } else {
//       result[key] = _copy(value);
//     }
//   }, {});

//   return result;
// }

// const _graphqlQuery = async model => {
// 	const response = await fetch('https://-.myshopify.com/api/graphql',
// 	{
// 		body: model,
// 		headers: {
// 			'X-Shopify-Storefront-Access-Token': '',
// 			'Content-Type': 'application/graphql',
// 		},
// 		method: 'post',
// 	});

// 	return response.json();
// }

// const getArticlesByQuery = params => {
//   let paramsString = [];
//   params = params ? params : {};
//   params.first = !params.first ? 100 : params.first;

//   for (let prop in params) {
//     paramsString.push(`${prop}:${params[prop]}`);
//   }

//   let data = `
//   {
//     entries(${paramsString.join(', ')}) {
//       edges {
//         node {
// 					id
// 					image {
// 						originalSrc
// 					}
//         }
//       }
//     }
//   }
//   `;

//   return _graphqlQuery(data);
// }

// const transformGraphqlData = data => {
//   return _copy(data);
// };
