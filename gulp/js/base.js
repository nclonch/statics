var $ = jQuery;
var mobile = window.innerWidth < 1024;

function capitalize(string)
{
	if (!string) {
		throw new Error('Cannot capitalize: A string must be provided.');
	}

	var array = []; string = string.split(' ');
	string.forEach(function(word){
		var i = 0, result = '';
		while (i < word.length) {
			if (/[a-zA-Z]/.test(word.charAt(i))) {
				result += word.charAt(i).toUpperCase();
				break;
			} else {
				result += word.charAt(i);
				i++;
			}
		}
		result += word.slice(i + 1);
		array.push(result);
	});
	return array.join(' ');
}

function cookieGet(name) {
	var nameEQ = name + '=';
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function cookieRemove(name) {
	document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function cookieSet(name, value, hours) {
	var expires = '';
	if (hours) {
		var date = new Date();
		date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
		expires = '; expires=' + date.toUTCString();
	}
	document.cookie = name + '=' + (value || '')  + expires + '; path=/';
}

function debounce(func, wait, immediate)
{
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

function didNotClick(element, parent)
{
	if (element && parent) {
		return !jQuery(element)[0].matches(parent) && jQuery(element).parents(parent).length < 1;
	} else {
		throw new Error('The element (i.e. `event.target`) and parent (a query selector string [e.g. "#parent"]) must be defined.');
	}
}

// function didNotClick(query, func)
// {
// 	return function() {
// 		var context = this, args = arguments, target = event.target;
// 		while (target) {
// 			if (target == query) {
// 				return false;
// 				break;
// 			} else {
// 				target = target.parentElement;
// 			}
// 		}
// 		func.apply(context, args);
// 	};
// }

function getParams(param)
{
	const params = JSON.parse('{"' + decodeURI(location.search.substring(1)).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
	return param ? params[param] : params;
}

function scale(variable, min, max, scale)
{
	return ((variable - min) / (max - min)) * scale;
	// For example, I need the window width
	// to be 0 at 200px and .5 at 800px.
	// scale(window.innerWidth, 200, 800, .5);
}

function throttle(func, wait, immediate)
{
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		if (!timeout) timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}
