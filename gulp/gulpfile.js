// npm install @babel/preset-env gulp gulp-autoprefixer gulp-babel browser-sync ansi-colors gulp-concat connect-modrewrite gulp-notify gulp-plumber gulp-rename gulp-sass gulp-uglify sass

// Include Gulp
    var gulp     = require('gulp'),

// Gulp plugins
    autoprefixer = require('gulp-autoprefixer'),
    babel        = require('gulp-babel'),
    browserSync  = require('browser-sync').create(),
    colors       = require('ansi-colors'),
    concat       = require('gulp-concat'),
    modRewrite   = require('connect-modrewrite'),
    notify       = require('gulp-notify'),
    plumber      = require('gulp-plumber'),
    rename       = require('gulp-rename'),
    scss         = require('gulp-sass')(require('sass')),
    uglify       = require('gulp-uglify'),
    srcPath      = '',
    distPath     = '../assets/';

// Input/Output paths and environment variables
    var input_paths = {
        scripts: ['js/base.js', 'js/layouts/**.js'],
        styles: ['scss/base.scss', 'scss/layouts/*.scss', '!scss/layouts/_*.scss']
    };

    var watch_paths = {
        scripts: input_paths.scripts,
        styles: ['scss/base.scss', 'scss/layouts/*.scss'],
        templates: ['../*.html']
    };

// Plugin options
    var scss_options = {
        outputStyle: 'compressed',
        errLogToConsole: false,
        onError: function(err) {
            notify().write(err);
            console.log(colors.red(err));
            process.stdout.write('\x07');
        }
    };

    var uglify_options = {
        mangle: false
    };


// Tasks
    function browserReload(done)
    {
        browserSync.reload();
        done();
    }

    function browserServe(done)
    {
        browserSync.init({
            server: {
                baseDir: '/Users/nateclonch/Sites/statics',
                middleware: [
                    modRewrite([
                        '!\\.\\w+$ /index.html [L]'
                    ])
                ],
                serveStaticOptions: {
                    extensions: ['html']
                }
            }
        });
        done();
    }

    function scriptsDebug()
    {
        return gulp
            .src(input_paths.scripts)
            .pipe(uglify(uglify_options));
    }

    function scripts()
    {
        return gulp
            .src(input_paths.scripts)
            .pipe(babel({
                presets: ['@babel/preset-env'],
            }))
            .pipe(rename(function(path){path.dirname = '';}))
            .pipe(concat('allthe.js'))
            .pipe(uglify(uglify_options))
            .pipe(gulp.dest(distPath))
            .pipe(browserSync.stream());
    }

    function styles()
    {
        return gulp
            .src(input_paths.styles)
            .pipe(scss(scss_options))
            .pipe(autoprefixer())
            .pipe(concat('allthe.css'))
            .pipe(rename(function(path){path.dirname = '';}))
            .pipe(gulp.dest(distPath))
            .pipe(browserSync.stream());
    }

// Public Tasks
    exports.build = gulp.parallel(scripts, styles);
    exports.default = gulp.series(gulp.parallel(scripts, styles), browserServe);

    // Task to find where JS error is coming from.
    exports.debugjs = gulp.series(scriptsDebug);

// Watch
    gulp.watch(watch_paths.scripts, gulp.parallel(scripts));
    gulp.watch(watch_paths.styles, gulp.parallel(styles));
    gulp.watch(watch_paths.templates).on('change', browserSync.reload);
